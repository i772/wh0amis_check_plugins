//     Dev: wh0ami
// License: Public Domain <https://unlicense.org>
// Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <cstdlib>
#include <filesystem>

int main() {
	using namespace std;
	string line, endoutput;
	fstream s("/proc/uptime", ios::in);
	getline(s, line);
	s.close();
	
	int days, hours, minutes, seconds, exitcode;
	int timestamp = stoi( line );
	div_t divresult;
	divresult = div(timestamp, 86400);
	days = divresult.quot;
	divresult = div(divresult.rem, 3600);
	hours = divresult.quot;
	divresult = div(divresult.rem, 60);
	minutes = divresult.quot;
	seconds = divresult.rem;
	
	string output = to_string(days) + " days, " + to_string(hours) + " hours, " + to_string(minutes) + " minutes and " + to_string(seconds) + " seconds | uptime=" + to_string(timestamp) + "s";
	if (filesystem::exists("/var/run/reboot-required")) {
		endoutput = "UPTIME WARNING - up since " + output + ", reboot required!";
		exitcode = 1;
	} else {
		endoutput = "UPTIME OK - up since " + output;
		exitcode = 0;
	}
	cout << endoutput << endl;
	return exitcode;
}
