#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import nagiosplugin
import os
from time import sleep

# data acquisition
class DiskIO(nagiosplugin.Resource):
	def getSectorSize(self):
		blocks = [block for block in os.listdir('/sys/block') if block.startswith("sd") or block.startswith("hd") or block.startswith("md") or block.startswith("sr")]
		disk = {}
		for dev in blocks:
			f = open("/sys/block/%s/queue/hw_sector_size" % dev, "r")
			disk["/dev/"+ dev] = int(f.readline().strip())
			f.close()
		return disk

	def getUsage(self):
		f = open("/proc/diskstats", "r")
		disk = {}
		SectorSize = self.getSectorSize()
		for line in f:
			dev = line.split()[2].strip()
			if "/dev/"+ dev in SectorSize:
				disk["/dev/"+ dev] = {}
				disk["/dev/"+ dev]["read"] = int(line.split()[3].strip())
				disk["/dev/"+ dev]["write"] = int(line.split()[7].strip())
		f.close()
		return disk

	def calc(self, val1, val2):
		return int(val2 - val1)

	def probe(self):
		stats1 = self.getUsage()
		sleep(1)
		stats2 = self.getUsage()

		for disk in stats2.keys():
			read = self.calc(stats1[disk]["read"], stats2[disk]["read"])
			yield nagiosplugin.Metric('%s_read' % disk, read, context='diskio')

			write = self.calc(stats1[disk]["write"], stats2[disk]["write"])
			yield nagiosplugin.Metric('%s_write' % disk, write, context='diskio')

# data presentation
class DiskIOSummary(nagiosplugin.Summary):
	def ok(self, results):
		text = ''
		for value in results:
			text = text + str(value) + ' IO/s, '
		return text[0:-2]


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
	argp = argparse.ArgumentParser(description=__doc__)
	argp.add_argument('-w', '--warning', metavar='RANGE', default='', help='return warning if <dev>_read or <dev>_write is outside RANGE')
	argp.add_argument('-c', '--critical', metavar='RANGE', default='', help='return critical if <dev>_read or <dev>_write is outside RANGE')
	args = argp.parse_args()
	check = nagiosplugin.Check(
		DiskIO(),
		nagiosplugin.ScalarContext('diskio', args.warning, args.critical),
		DiskIOSummary())
	check.main()


if __name__ == '__main__':
    main()
