#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <iterator>
#include <unistd.h>
#include <iomanip>

using namespace std;

bool containsString(string querystring, string data);
static inline string &ltrim(string &s);
bool BothAreSpaces(char lhs, char rhs);
double calcMbit(long input);

int main(int argc, char *argv[]) {

    int warning, critical, exitcode = 0, nolimit = 0;
    string status = "NETWORK OK - ";

    if (argc == 1) {
        nolimit = 1;
    }
    else if (argc < 3) {
        cout << "USAGE: check_network_mbps <warning> <critical>" << endl;
        return 1;
    } else {
        warning = atoi(argv[1]);
        critical = atoi(argv[2]);
    }

    string line;
    vector<long> values_1;
    vector<long> values_2;
    vector<long> values;
    vector<string> interfaces;

    for (int x = 0; x < 2; x++) {

        fstream file("/proc/net/dev", ios::in); //test

        while(getline(file, line)) { //parse /proc/net/dev file, it was annoying.

            if (containsString("|", line) || containsString("lo:", line)) {
                continue;
            }

            string::iterator new_end = unique(line.begin(), line.end(), BothAreSpaces);
            line.erase(new_end, line.end());
            line = ltrim(line);
            size_t pos = line.find(":");
            if (x==0) {
                interfaces.push_back(line.substr(0, pos));
            }
            line = line.substr(pos+2);
            stringstream ss(line);
            istream_iterator<string> begin(ss);
            istream_iterator<string> end;
            vector<string> vstrings(begin, end);
            for (int i=0; i<vstrings.size(); i++) {
                if (i != 0 && i != 7) { //columns you want
                    continue;
                }
                if (x == 0) {
                    values_1.push_back(stol(vstrings[i]));
                }
                else if (x == 1) {
                    values_2.push_back(stol(vstrings[i]));
                }

            }
        }
        file.close();
        sleep(1);
    }

        for (int i=0; i < values_1.size(); i++) {
            values.push_back(values_2[i]-values_1[i]);
        }

    if (nolimit == 0) {
        for (int i=0; i<values.size(); i++) {
            if (values[i] > critical) {
                exitcode = 2;
            }
            else if (values[i] > warning) {
                exitcode = 1;
            }
        }

        if (exitcode == 2) {
            status = "NETWORK CRITICAL - ";
        }
        else if (exitcode == 1) {
            status = "NETWORK WARNING - ";
        }
    }

    cout << status;
    cout << fixed << setprecision(2);

    for (int i=0, a=0; i< interfaces.size(); i++, a++) {
        if (i != interfaces.size()-1) {
            cout << interfaces[i] << "_download is " << calcMbit(values[i+a]) << " MBit/s, " << interfaces[i] << "_upload is " << calcMbit(values[i+a+1]) << " MBit/s, ";
        } else {
            cout << interfaces[i] << "_download is " << calcMbit(values[i+a]) << " Mbit/s, " << interfaces[i] << "_upload is " << calcMbit(values[i+a+1]) << " Mbit/s | ";
        }
    }

    cout << fixed << setprecision(4);

    for (int i=0, a=0; i< interfaces.size(); i++, a++) {
            cout << interfaces[i] << "_download=" << calcMbit(values[i+a]) << " " << interfaces[i] << "_upload=" << calcMbit(values[i+a+1]) << " ";
    }
    cout << endl;

    return exitcode;
}

bool containsString(string querystring, string data) {
    return data.find(querystring) != string::npos;
}

static inline string &ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(),
            not1(ptr_fun<int, int>(isspace))));
    return s;
}

bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }

double calcMbit(long input) {
    return 1.0*((input*8)/1024)/1024; //multiply by 1 to convert it to double
}
