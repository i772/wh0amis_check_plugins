#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <filesystem>
#include <vector>

using namespace std;

bool digitCheck(string str);
bool containsString(string querystring, string data);

int main(int argc, char *argv[]) {

    int warning, critical, nolimit = 0;

    if (argc == 1) {
        nolimit = 1;
    }
    else if (argc < 3) {
        cerr << "USAGE: check_procs <warning> <critical>" << endl;
        return 1;
    }
    else {
        warning = atoi(argv[1]);
        critical = atoi (argv[2]);
    }

    vector<int> pids;
    vector<string> filebuffer;
    string tmp;
    int processcount, zombieprocess = 0, sshsessions = 0, exitcode = 0;

    for (const auto & entry : filesystem::directory_iterator("/proc")) {
        tmp = entry.path();
        tmp.erase(0, 6);
        if (digitCheck(tmp)) {
            pids.push_back(atoi(tmp.c_str()));
        }
    }
    processcount = pids.size();
    for (int pid : pids) {
        try {
            fstream pidstr("/proc/" + to_string(pid) + "/cmdline", ios::in);
            getline(pidstr, tmp);
            if (containsString("@pts/", tmp)) {
                sshsessions++;
            }
            pidstr.close();
            fstream statestr("/proc/" + to_string(pid) + "/status", ios::in);
            for (int i=0; i<2; i++) {
                getline(statestr, tmp);
                filebuffer.push_back(tmp);
            }
            statestr.close();
            if (containsString("Z", filebuffer[1])) {
                zombieprocess++;
            }
        }
        catch(...) {
            continue;
        }
    }
    if (nolimit == 0) {
        if (processcount > critical || zombieprocess > critical || sshsessions > critical) {
            cout << "PROCS CRITICAL - " << to_string(processcount) << " processes, " << to_string(zombieprocess) << " zombie processes, " << to_string(sshsessions) << " SSH sessions | " << "processes=" << to_string(processcount) << " zombieprocs=" << to_string(zombieprocess) << " ssh_sessions=" << to_string(sshsessions) << endl;
            exitcode = 2;
        }
        else if (processcount > warning || zombieprocess > warning || sshsessions > warning) {
            cout << "PROCS WARNING - " << to_string(processcount) << " processes, " << to_string(zombieprocess) << " zombie processes, " << to_string(sshsessions) << " SSH sessions | " << "processes=" << to_string(processcount) << " zombieprocs=" << to_string(zombieprocess) << " ssh_sessions=" << to_string(sshsessions) << endl;
            exitcode = 1;
        }
        else {
            cout << "PROCS OK - " << to_string(processcount) << " processes, " << to_string(zombieprocess) << " zombie processes, " << to_string(sshsessions) << " SSH sessions | " << "processes=" << to_string(processcount) << " zombieprocs=" << to_string(zombieprocess) << " ssh_sessions=" << to_string(sshsessions) << endl;
            exitcode = 0;
        }
    } else {
        cout << "PROCS OK - " << to_string(processcount) << " processes, " << to_string(zombieprocess) << " zombie processes, " << to_string(sshsessions) << " SSH sessions | " << "processes=" << to_string(processcount) << " zombieprocs=" << to_string(zombieprocess) << " ssh_sessions=" << to_string(sshsessions) << endl;
        exitcode = 0;
    }
    return exitcode;
}

bool digitCheck(string str) {
    return str.find_first_not_of("0123456789") == string::npos;
}

bool containsString(string querystring, string data) {
    return data.find(querystring) != string::npos;
}
