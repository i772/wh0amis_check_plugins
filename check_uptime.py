#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import os
import sys

# get the uptime from /proc/uptime
f = open("/proc/uptime", "r")
uptime = int(round(float(f.readline().split()[0]), 0)) # first part of /proc/uptime
f.close()

# transform the uptime in a readable format
days, seconds	 = divmod(uptime, 86400)
hours, seconds	 = divmod(seconds, 3600)
minutes, seconds = divmod(seconds, 60)
output = "%s days, %s hours, %s minutes and %s seconds" % (str(days), str(hours), str(minutes), str(seconds))

# return a warning, if a reboot is required
if os.path.exists("/var/run/reboot-required"):
	output = "UPTIME WARNING - up since %s, reboot required!" % (str(output))
	exitcode = 1
else:
	output = "UPTIME OK - up since %s" % (str(output))
	exitcode = 0

# append the uptime as readable value
output = output + " | uptime=%ss" % uptime

# print the status
print(output)

sys.exit(exitcode)
