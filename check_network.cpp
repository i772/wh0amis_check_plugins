#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <iterator>
#include <unistd.h>
#include <iomanip>

using namespace std;

vector<long> parseProcNet(string path, int column1, int column2);
double calc(long input);
static inline string &ltrim(string &s);
bool BothAreSpaces(char lhs, char rhs);
bool containsString(string querystring, string data);
vector<string> getDevices();

int main(int argc, char *argv[]) {
  int nolimit = 0, exitcode = 0, warning_pps, critical_pps, warning_mbit, critical_mbit;
  vector<long> stats_packets_1, stats_packets_2, stats_mbit_1, stats_mbit_2;
  vector<string> device_list;
  string status = "NETWORK OK - ";

  if (argc == 1) {
    nolimit = 1;
  }
  else if (argc < 5) {
    cout << "USAGE: check_network <warning_pps> <critical_pps> <warning_mbits> <critical_mbits>" << endl;
    return 1;
  }
  else {
    warning_pps = stoi(argv[1]);
    critical_pps = stoi(argv[2]);
    warning_mbit = stoi(argv[3]);
    critical_mbit = stoi(argv[4]);
  }

  device_list = getDevices();

  stats_packets_1 = parseProcNet("/proc/net/dev", 2, 10);
  stats_mbit_1 = parseProcNet("/proc/net/dev", 1, 9);
  sleep(1);
  stats_packets_2 = parseProcNet("/proc/net/dev", 2, 10);
  stats_mbit_2 = parseProcNet("/proc/net/dev", 1, 9);

  for (int i=0; i<stats_packets_1.size(); i++) {
    stats_packets_1[i] = stats_packets_2[i] - stats_packets_1[i];
    stats_mbit_1[i] = stats_mbit_2[i] - stats_mbit_2[i];
  }

  if (nolimit == 0) {
    for (int i=0; i<stats_packets_1.size(); i++) {
      if (stats_packets_1[i] >= critical_pps || calc(stats_mbit_1[i]) >= critical_mbit) {
        status = "NETWORK CRITICAL - ";
        exitcode = 2;
      }
      else if (stats_packets_1[i] >= warning_pps || calc(stats_mbit_1[i]) >= warning_mbit) {
        status = "NETWORK WARNING - ";
        exitcode = 1;
      }
      else {
        continue;
      }
    }
  }

  if (exitcode == 0) {
    status = "NETWORK OK - normal workload | ";
  }

  cout << status;
  cout << fixed << setprecision(2);

  if (exitcode != 0) {
    for (int i=0, a=0; i < device_list.size(); i++, a += 2) {
      if (i != device_list.size()-1) {
        cout << device_list[i] << "_pps_download is " << stats_packets_1[a] << " packets/s, ";
        cout << device_list[i] << "_pps_upload is " << stats_packets_1[a+1] << " packets/s, ";
        cout << device_list[i] << "_mbit_download is " << calc(stats_mbit_1[a]) << " mbit/s, ";
        cout << device_list[i] << "_upload is_mbit " << calc(stats_mbit_1[a+1]) << " mbit/s, ";
      } else {
        cout << device_list[i] << "_pps_download is " << stats_packets_1[a] << " packets/s, ";
        cout << device_list[i] << "_pps_upload is " << stats_packets_1[a+1] << " packets/s, ";
        cout << device_list[i] << "_mbit_download is " << calc(stats_mbit_1[a]) << " mbit/s, ";
        cout << device_list[i] << "_mbit_upload is " << calc(stats_mbit_1[a+1]) << " mbit/s | ";
      }
    }
  }
  cout << fixed << setprecision(4);

  for (int i=0, a=0; i < device_list.size(); i++, a += 2) {
    if (i != device_list.size()-1) {
      cout << device_list[i] << "_pps_download=" << stats_packets_1[a] << " ";
      cout << device_list[i] << "_pps_upload=" << stats_packets_1[a+1] << " ";
      cout << device_list[i] << "_mbit_download=" << calc(stats_mbit_1[a]) << " ";
      cout << device_list[i] << "_mbit_upload=" << calc(stats_mbit_1[a+1]) << " ";
    } else {
      cout << device_list[i] << "_pps_download=" << stats_packets_1[a] << " ";
      cout << device_list[i] << "_pps_upload=" << stats_packets_1[a+1] << " ";
      cout << device_list[i] << "_mbit_download=" << calc(stats_mbit_1[a]) << " ";
      cout << device_list[i] << "_mbit_upload=" << calc(stats_mbit_1[a+1]);
    }
  }
  cout << endl;

  return exitcode;
}

static inline string &ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(),
            not1(ptr_fun<int, int>(isspace))));
    return s;
}

bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }

bool containsString(string querystring, string data) {
    return data.find(querystring) != string::npos;
}

vector<long> parseProcNet(string path, int column1, int column2) {
  vector<long> outvec;
  string line;
  fstream file(path, ios::in);
  while(getline(file, line)) {
    if (!containsString("lo:", line) && !containsString("|", line) && !containsString("tap", line) && !containsString("veth", line) && !containsString("fw", line)) {
      string::iterator new_end = unique(line.begin(), line.end(), BothAreSpaces);
      line.erase(new_end, line.end());
      line = ltrim(line);
      stringstream ss(line);
      istream_iterator<string> begin(ss);
      istream_iterator<string> end;
      vector<string> vstrings(begin, end);
      for (int i=0; i<vstrings.size(); i++) {
        if ((i == column1 || i == column2)) {
          outvec.push_back(stol(vstrings[i]));
        }
      }
    } else {
      continue;
    }
  }
  return outvec;
}

vector<string> getDevices() {
  vector<string> outvec;
  string line;
  fstream file("/proc/net/dev", ios::in);
  while(getline(file, line)) {
    if (!containsString("lo:", line) && !containsString("|", line) && !containsString("tap", line) && !containsString("veth", line) && !containsString("fw", line)) {
      string::iterator new_end = unique(line.begin(), line.end(), BothAreSpaces);
      line.erase(new_end, line.end());
      line = ltrim(line);
      stringstream ss(line);
      istream_iterator<string> begin(ss);
      istream_iterator<string> end;
      vector<string> vstrings(begin, end);
      vstrings[0].pop_back();
      outvec.push_back(vstrings[0]);
    }
  }
  return outvec;
}

double calc(long input) {
  return (double)(((input*8)/1024)/1024);
}
