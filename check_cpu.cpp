#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <iterator>
#include <unistd.h>
#include <iomanip>

using namespace std;

vector<int> getUsage();
bool BothAreSpaces(char lhs, char rhs);
bool containsString(string querystring, string data);
int getCoreCount();

int main(int argc, char *argv[]) {
    
    int warning, critical, total = 0, corecount = 0;
    vector<int> values_1;
    vector<int> values_2;
    vector<int> values;
    vector<double> percentages;
    double usage, totalperc;
    string status;
    int nolimit = 0;

    if (argc == 1) {
        nolimit = 1;
    }
    else if (argc < 3) {
        cout << "USAGE: check_cpu <warning> <critical>";
        return 1;
    } else {
        warning = atoi(argv[1]);
        critical = atoi(argv[2]);
    }
    
    values_1 = getUsage();
    sleep(1);
    values_2 = getUsage();

    for (int i = 0; i<values_1.size(); i++) {
        values.push_back(values_2[i]-values_1[i]);
        total = total + values[i];
    }

    for (int i=0; i<values.size(); i++) {
        percentages.push_back((values[i]*100.0)/total);
        totalperc = totalperc + percentages[i];
    }

    usage = totalperc - percentages[3]; //percentages[3] = idle
    corecount = getCoreCount();

    //output data
    if (nolimit == 0) {
        if (usage > critical) {
            status = "CPU CRITICAL - ";
        }
        else if (usage > warning) {
            status = "CPU WARNING - ";
        }
        else {
            status = "CPU OK - ";
        }
    } else {
        status = "CPU OK - ";
    }

    cout << status << fixed << setprecision(2)
        << "usage is " << usage;
    
    cout << fixed << setprecision(4) << "% | cores=" << corecount
        << " user=" << percentages[0]
        << "% nice=" << percentages[1]
        << "% system=" << percentages[2] 
        << "% idle=" << percentages[3] 
        << "% iowait=" << percentages[4]
        << "% irq=" << percentages[5]
        << "% softirq=" << percentages[6]
        << "% steal=" << percentages[7] 
        << "% usage=" << usage
        << "%" << endl;
}

vector<int> getUsage() {
    vector<int> valtemp;
    fstream file("/proc/stat", ios::in);
    string line;
    getline(file, line);
    string::iterator new_end = unique(line.begin(), line.end(), BothAreSpaces);
    line.erase(new_end, line.end());
    line.erase(0,4);
    stringstream ss(line);
    istream_iterator<string> begin(ss);
    istream_iterator<string> end;
    vector<string> vstrings(begin, end);
    for (int i=0; i<vstrings.size(); i++) {
        valtemp.push_back(stoi(vstrings[i]));
    }
    file.close();
    return valtemp;
}

bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }

int getCoreCount() {
    fstream file("/proc/stat", ios::in);
    string tmp;
    int counter = 0;
    while(getline(file, tmp)) {
        if (containsString("cpu", tmp)) {
            counter++;
        } else {
            continue;
        }
    }
    return counter-1;
}

bool containsString(string querystring, string data) {
    return data.find(querystring) != string::npos;
}