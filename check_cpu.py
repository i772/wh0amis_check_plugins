#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import nagiosplugin
from multiprocessing import cpu_count
from time import sleep

# data acquisition
class CPU(nagiosplugin.Resource):
	def getUsage(self):
		data = {}
		f = open("/proc/stat", "r")
		data = f.readline().split()
		f.close()
		return data

	def probe(self):
		stats1 = self.getUsage()
		sleep(1)
		stats2 = self.getUsage()

		#Calculate CPU values in percent
		times = {}
		times["user"]		= float(int(stats2[1]) - int(stats1[1]))
		times["nice"]		= float(int(stats2[2]) - int(stats1[2]))
		times["system"]		= float(int(stats2[3]) - int(stats1[3]))
		times["idle"]		= float(int(stats2[4]) - int(stats1[4]))
		times["iowait"]		= float(int(stats2[5]) - int(stats1[5]))
		times["irq"]		= float(int(stats2[6]) - int(stats1[6]))
		times["softirq"]	= float(int(stats2[7]) - int(stats1[7]))
		times["steal"]		= float(int(stats2[8]) - int(stats1[8]))

		total = times["user"] + times["nice"] + times["system"] + times["idle"] + times["iowait"] + times["steal"] + times["irq"] + times["softirq"]

		percent = {}
		percent["user"]		= round(times["user"]	 * 100 / float(total), 2)
		percent["nice"]		= round(times["nice"]	 * 100 / float(total), 2)
		percent["system"]	= round(times["system"]	 * 100 / float(total), 2)
		percent["idle"]		= round(times["idle"]	 * 100 / float(total), 2)
		percent["iowait"]	= round(times["iowait"]	 * 100 / float(total), 2)
		percent["irq"]		= round(times["irq"]	 * 100 / float(total), 2)
		percent["softirq"]	= round(times["softirq"] * 100 / float(total), 2)
		percent["steal"]	= round(times["steal"]	 * 100 / float(total), 2)
		usage = round(float(sum(percent.values()) - percent["idle"]), 2)

		yield nagiosplugin.Metric('usage', usage, context='usage', uom='%')

		for key, val in percent.items():
			yield nagiosplugin.Metric(key, val, context='percent', uom='%')

		yield nagiosplugin.Metric('cores', cpu_count(), context='meta')


# data presentation
class CPUSummary(nagiosplugin.Summary):
	def ok(self, results):
		return 'usage is %s' % (str(results["usage"].metric))


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
	argp = argparse.ArgumentParser(description=__doc__)
	argp.add_argument('-w', '--warning', metavar='RANGE', default='', help='return warning if usage is outside RANGE')
	argp.add_argument('-c', '--critical', metavar='RANGE', default='', help='return critical if usage is outside RANGE')
	args = argp.parse_args()
	check = nagiosplugin.Check(
		CPU(),
		nagiosplugin.ScalarContext('usage', args.warning, args.critical), nagiosplugin.ScalarContext('percent'), nagiosplugin.ScalarContext('times'), nagiosplugin.ScalarContext('meta'),
		CPUSummary())
	check.main()


if __name__ == '__main__':
    main()
