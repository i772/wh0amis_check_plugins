#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import nagiosplugin
import re
from time import sleep

# data acquisition
class Network(nagiosplugin.Resource):
	def getUsage(self):
		f = open("/proc/net/dev", "r")
		network = {}
		for line in f:
			content = line.split()
			if ":" in content[0] and "lo:" not in content[0] and not re.match(r"^fw.+$|^veth.+$|^tap.+$", content[0]):
				interface 			= content[0][:-1]
				network[interface] 		= {}
				network[interface]["download"]	= content[1]
				network[interface]["upload"]	= content[9]
		f.close()
		return network

	def calc(self, val1, val2):
		return round((((int(val2) - int(val1)) * 8) / 1024) / 1024, 2)

	def probe(self):
		stats1 = self.getUsage()
		sleep(1)
		stats2 = self.getUsage()

		for interface in stats2.keys():
			download = self.calc(stats1[interface]["download"], stats2[interface]["download"])
			yield nagiosplugin.Metric('%s_download' % interface, download, context='network')

			upload = self.calc(stats1[interface]["upload"], stats2[interface]["upload"])
			yield nagiosplugin.Metric('%s_upload' % interface, upload, context='network')


# data presentation
class NetworkSummary(nagiosplugin.Summary):
	def ok(self, results):
		text = ''
		for value in results:
			text = text + str(value) + ' MBit/s, '
		return text[0:-2]


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
	argp = argparse.ArgumentParser(description=__doc__)
	argp.add_argument('-w', '--warning', metavar='RANGE', default='', help='return warning if <dev>_upload or <dev>_download is outside RANGE')
	argp.add_argument('-c', '--critical', metavar='RANGE', default='', help='return critical if <dev>_upload or <dev>_download is outside RANGE')
	args = argp.parse_args()
	check = nagiosplugin.Check(
		Network(),
		nagiosplugin.ScalarContext('network', args.warning, args.critical),
		NetworkSummary())
	check.main()


if __name__ == '__main__':
    main()
