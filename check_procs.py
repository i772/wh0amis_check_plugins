#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import nagiosplugin
import os

# data acquisition
class Procs(nagiosplugin.Resource):
	def probe(self):
		pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

		processcount = len(pids)
		zombieprocs = 0
		ssh_sessions = 0

		for pid in pids:
			try:
				# if /proc/PID/cmdline contains "@pts/", we have a SSH session
				f = open("/proc/%s/cmdline" % pid, "r")
				line = f.readline()
				f.close
				if "@pts/" in line:
					ssh_sessions = ssh_sessions + 1

				# if the state of a process in /proc/PID/status is "Z", we have a zombie process
				f = open("/proc/%s/status" % pid, "r")
				for line in f:
					state = line.split()
					if state[0] == "State:":
						if state[1] == "Z":
							zombieprocs = zombieprocs + 1
						break
				f.close
			except IOError:
				# only if the process stops
				continue

		yield nagiosplugin.Metric('processes', processcount, context='processes')
		yield nagiosplugin.Metric('zombieprocs', zombieprocs, context='zombieprocs')
		yield nagiosplugin.Metric('ssh_sessions', ssh_sessions, context='ssh_sessions')


# data presentation
class ProcsSummary(nagiosplugin.Summary):
	def ok(self, results):
		return '%s processes, %s zombie processes, %s SSH sessions' % (str(results["processes"].metric.value), str(results["zombieprocs"].metric.value), str(results["ssh_sessions"].metric.value))


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
	argp = argparse.ArgumentParser(description=__doc__)
	argp.add_argument('-w', '--warning', metavar='RANGE', default='', help='return warning if processes is outside RANGE')
	argp.add_argument('-c', '--critical', metavar='RANGE', default='', help='return critical if processes is outside RANGE')
	argp.add_argument('-y', '--zombiewarning', metavar='RANGE', default='', help='return warning if zombieprocs is outside RANGE')
	argp.add_argument('-z', '--zombiecritical', metavar='RANGE', default='', help='return critical if zombieprocs is outside RANGE')
	argp.add_argument('-s', '--sshwarning', metavar='RANGE', default='', help='return warning if ssh_sessions is outside RANGE')
	argp.add_argument('-t', '--sshcritical', metavar='RANGE', default='', help='return critical if ssh_sessions is outside RANGE')
	args = argp.parse_args()
	check = nagiosplugin.Check(
		Procs(),
		nagiosplugin.ScalarContext('processes', args.warning, args.critical), nagiosplugin.ScalarContext('zombieprocs', args.zombiewarning, args.zombiecritical), nagiosplugin.ScalarContext('ssh_sessions', args.sshwarning, args.sshcritical),
		ProcsSummary())
	check.main()


if __name__ == '__main__':
    main()
