#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#     Dev: wh0ami
# License: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

import argparse
import nagiosplugin

# data acquisition
class Memory(nagiosplugin.Resource):
	def probe(self):
		f = open("/proc/meminfo", "r")

		memory = {}
		for line in f:
			buffer = line.split()
			if buffer[0] == "MemTotal:":
				memory["total"]   = float(round(int(buffer[1])/1024, 2))
			elif buffer[0] == "MemFree:":
				memory["free"]	  = float(round(int(buffer[1])/1024, 2))
			elif buffer[0] == "Slab:":
				memory["slab"]	  = float(round(int(buffer[1])/1024, 2))
			elif buffer[0] == "Cached:":
				memory["cached"]  = float(round(int(buffer[1])/1024, 2))
			elif buffer[0] == "Buffers:":
				memory["buffers"] = float(round(int(buffer[1])/1024, 2))
		f.close()

		memory["free"]	= float(round(memory["free"] + memory["cached"] + memory["buffers"] + memory["slab"], 2))
		memory["used"]	= float(round(memory["total"] - memory["free"], 2))

		usage = float(round(memory["used"] / (memory["total"] / 100), 2))

		yield nagiosplugin.Metric('usage', usage, context='usage', uom='%')

		for key, val in memory.items():
			yield nagiosplugin.Metric(key, val, context='memory')


# data presentation
class MemorySummary(nagiosplugin.Summary):
	def ok(self, results):
		return 'usage is %s' % (str(results["usage"].metric))


# runtime environment and data evaluation
@nagiosplugin.guarded
def main():
	argp = argparse.ArgumentParser(description=__doc__)
	argp.add_argument('-w', '--warning', metavar='RANGE', default='', help='return warning if usage is outside RANGE')
	argp.add_argument('-c', '--critical', metavar='RANGE', default='', help='return critical if usage is outside RANGE')
	args = argp.parse_args()
	check = nagiosplugin.Check(
		Memory(),
		nagiosplugin.ScalarContext('usage', args.warning, args.critical), nagiosplugin.ScalarContext('memory'),
		MemorySummary())
	check.main()


if __name__ == '__main__':
    main()
