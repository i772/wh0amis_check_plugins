for cppFile in $(ls *.cpp)
do
	echo "Compiling ${cppFile}..."

	outputFile=$(cut -d. -f1 <<< ${cppFile})
	g++ $cppFile -o $outputFile -lstdc++fs -std=c++17
	rm -f ${outputFile}.cppx

	echo "Done!"
	echo
done
