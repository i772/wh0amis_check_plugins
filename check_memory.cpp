//     Dev: wh0ami
// License: Public Domain <https://unlicense.org>
// Project: https://codeberg.org/wh0ami/wh0amis_check_plugins

#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <sys/sysinfo.h>
#include <regex>

using namespace std;

double getValue(string & str) {
	regex rgx("([a-zA-Z]+:[ \t]*)([0-9]+)( kB)");
	smatch matches;

	regex_search(str, matches, rgx);

	double output = (double)atoi(matches[2].str().c_str()) / 1024;

	return output;
}

string formatNumber(double & value) {
	char * buf = new char[200];
	sprintf(buf, "%.2f", (int)(value * 100 + 0.5) / 100.0);

	string output = string(buf);
	delete buf;

	return output;
}


int main(int argc, char *argv[]) {
	double memtotal, memfree, memavailable, buffers, cached, slab, memused, usage;
	double warning, critical;
	int exitcode, nolimit = 0;
	string line, perfdata;

	if (argc == 1) {
        nolimit = 1;
    }
    else if (argc != 3) {
		cout << "MEMORY ARGUMENT ERROR - USAGE: check_memory <warning> <critical>" << endl;
		return 3;
	} else {
		warning = stod(argv[1]);
		critical = stod(argv[2]);
	}

	fstream in("/proc/meminfo", ios::in);
	for (int linecounter = 1; linecounter <= 23; linecounter++) {
		getline(in, line);

		switch(linecounter) {
			case 1:	memtotal = getValue(line);
				break;
			case 2: memfree = getValue(line);
				break;
			case 3: memavailable = getValue(line);
				break;
			case 4: buffers = getValue(line);
				break;
			case 5: cached = getValue(line);
				break;
			case 23: slab = getValue(line);
				 break;
		}
	}
	in.close();

	memused = memtotal - memavailable;
	usage = (memused / memtotal) * 100;

	perfdata = " - usage is "+ formatNumber(usage) +"% | buffers="+ formatNumber(buffers) +"MB cached="+ formatNumber(cached) +"MB free="+ formatNumber(memfree) +"MB slab="+ formatNumber(slab) +"MB total="+ formatNumber(memtotal) +"MB usage="+ formatNumber(usage) +"%;"+ formatNumber(warning) +";"+ formatNumber(critical) +" used="+ formatNumber(memused) +"MB";
    if (nolimit == 0) {
        if (usage >= critical) {
            perfdata = "MEMORY CRITICAL"+ perfdata;
            exitcode = 2;
        } else if (usage >= warning) {
            perfdata = "MEMORY WARNING"+ perfdata;
            exitcode = 1;
        } else {
            perfdata = "MEMORY OK"+ perfdata;
            exitcode = 0;
        }
    } else {
        perfdata = "MEMORY OK"+ perfdata;
    }

	cout << perfdata << endl;

	return exitcode;


	cout << "Tot: "+ formatNumber(memtotal) << endl;
	cout << "Free: "+ formatNumber(memfree) << endl;
	cout << "Ava: "+ formatNumber(memavailable) << endl;
	cout << "Buff: "+ formatNumber(buffers) << endl;
	cout << "Cach: "+ formatNumber(cached) << endl;
	cout << "Slab: "+ formatNumber(slab) << endl;
	cout << "perc: "+ formatNumber(usage) +"%" << endl;

	return exitcode;
}
