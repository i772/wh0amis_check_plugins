#include <iostream>
#include <fstream>
#include <ios>
#include <string>
#include <filesystem>
#include <vector>
#include <algorithm>
#include <cctype>
#include <cstring>
#include <iterator>
#include <unistd.h>
#include <iomanip>

using namespace std;

vector<long> parseDiskstat(vector<string> devices, string path, int column1, int column2);
vector<string> getDevices();
vector<long> getBlocksizes(vector<string> v);
static inline string &ltrim(string &s);
bool BothAreSpaces(char lhs, char rhs);
double calc(long diff, long blocksize);

int main(int argc, char *argv[]) {
  int exitcode = 0, warning_iops, warning_mbps, critical_iops, critical_mbps, nolimit = 0;
  vector<string> device_list;
  vector<long> stats_iops_1, stats_iops_2, stats_mbps_1, stats_mbps_2, blocksizes;
  string status = "DISK OK - ";

  if (argc == 1) {
    nolimit = 1;
  }
  else if (argc < 3) {
    cout << "USAGE: check_disk <warning_iops> <critical_iops> <warning_mbps> <critical_mbps>" << endl;
    return 1;
  }
  else {
    warning_iops = stoi(argv[1]);
    critical_iops = stoi(argv[2]);
    warning_mbps = stoi(argv[3]);
    critical_iops = stoi(argv[4]);
  }

  device_list = getDevices();
  blocksizes = getBlocksizes(device_list);
  stats_iops_1 = parseDiskstat(device_list, "/proc/diskstats", 3, 7);
  stats_mbps_1 = parseDiskstat(device_list, "/proc/diskstats", 5, 9);
  sleep(1);
  stats_iops_2 = parseDiskstat(device_list, "/proc/diskstats", 3, 7);
  stats_mbps_2 = parseDiskstat(device_list, "/proc/diskstats", 5, 9);

  //calculate diffs into stats_<name>_1

  for (int i=0; i<stats_iops_1.size(); i++) { //mbps and iops vectors should have exactly the same size
    stats_iops_1[i] = stats_iops_2[i] - stats_iops_1[i];
    stats_mbps_1[i] = stats_mbps_2[i] - stats_mbps_1[i];
  }

  if (nolimit == 0) {
    for (int i=0; i<stats_iops_1.size(); i++) {
      if (stats_iops_1[i] > critical_iops || calc(stats_mbps_1[i], blocksizes[i]) > critical_mbps) {
        status = "DISK CRITICAL - ";
        exitcode = 2;
      }
      else if (stats_iops_1[i] > warning_iops || calc(stats_mbps_1[i], blocksizes[i]) > warning_mbps) {
        status = "DISK WARNING - ";
        exitcode = 1;
      }
      else {
        continue;
      }
    }
  }

  if (exitcode == 0) {
    status = "DISK OK - normal workload | ";
  }

  cout << status;

  if (exitcode != 0) {
    for (int i=0, a=0; i<device_list.size(); i++, a+=2) {
      cout << device_list[i] << "_iops_read is " << stats_iops_1[a] << " IO/s, "
           << device_list[i] << "_iops_write is " << stats_iops_1[a+1] << " IO/s, ";
    }
    cout << fixed << setprecision(2);

    for (int i=0, a=0; i<device_list.size(); i++, a+=2) {
      if (i != device_list.size()-1) {
        cout << device_list[i] << "_mibs_read is " << calc(stats_mbps_1[a], blocksizes[i]) << " MiB/s, "
             << device_list[i] << "_mibs_write is " << calc(stats_mbps_1[a+1], blocksizes[i]) << " MiB/s, ";
      } else {
        cout << device_list[i] << "_mibs_read is " << calc(stats_mbps_1[a], blocksizes[i]) << " MiB/s, "
             << device_list[i] << "_mibs_write is " << calc(stats_mbps_1[a+1], blocksizes[i]) << " MiB/s | ";
      }
    }
  }
  cout << fixed << setprecision(4);

  for (int i=0, a=0; i<device_list.size(); i++, a+=2) {
    cout << "'" << device_list[i] << "_iops_read'=" << stats_iops_1[a] << " "
         << "'" << device_list[i] << "_iops_write'=" << stats_iops_1[a+1] << " ";
  }

  for (int i=0, a=0; i<device_list.size(); i++, a+=2) {
    if (i != device_list.size()-1) {
      cout << "'" << device_list[i] << "_mibs_read'=" << calc(stats_mbps_1[a], blocksizes[i]) << " "
           << "'" << device_list[i] << "_mibs_write'=" << calc(stats_mbps_1[a+1], blocksizes[i]) << " ";
    } else {
      cout << "'" << device_list[i] << "_mibs_read'=" << calc(stats_mbps_1[a], blocksizes[i]) << " "
           << "'" << device_list[i] << "_mibs_write'=" << calc(stats_mbps_1[a+1], blocksizes[i]) << "";
    }
  }

  cout << endl;

  return exitcode;
}

bool containsString(string querystring, string data) {
    return data.find(querystring) != string::npos;
}

static inline string &ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(),
            not1(ptr_fun<int, int>(isspace))));
    return s;
}

bool BothAreSpaces(char lhs, char rhs) { return (lhs == rhs) && (lhs == ' '); }

vector<long> parseDiskstat(vector<string> devices, string path, int column1, int column2) {
  vector<long> outvec;
  string line;
  bool isDevice = false;
  fstream file(path, ios::in);
  while(getline(file, line)) {
    string::iterator new_end = unique(line.begin(), line.end(), BothAreSpaces);
    line.erase(new_end, line.end());
    line = ltrim(line);
    stringstream ss(line);
    istream_iterator<string> begin(ss);
    istream_iterator<string> end;
    vector<string> vstrings(begin, end);
    for (int i=0; i<devices.size(); i++) {
      if (containsString(devices[i] + " ", line)) { // space for exclusion of sda1, sda2 etc.
        isDevice = true;
        break;
      } else {
        isDevice = false;
      }
    }
    if (isDevice == true) {
      for (int i=0; i<vstrings.size(); i++) {
        if ((i == column1 || i == column2)) {
          outvec.push_back(stol(vstrings[i]));
        }
      }
    } else {
      continue;
    }
  }
  return outvec;
}

vector<string> getDevices() {
  vector<string> outvec;
  string path = "/sys/block/", secpath;
  for (const auto & entry : filesystem::directory_iterator(path)) {
    if (!containsString("sd", entry.path()) && !containsString("hd", entry.path()) && !containsString("md", entry.path()) && !containsString("sr", entry.path())) {
      continue;
    } else {
      outvec.push_back(filesystem::path(entry.path()).filename().string());
    }
  }
  return outvec;
}

vector<long> getBlocksizes(vector<string> v) {
  vector<long> outvec;
  string basic_path = "/sys/block/";
  string second_path = "/queue/hw_sector_size";
  string tmp;
  for (int i=0; i<v.size(); i++) {
    fstream file(basic_path + v[i] + second_path, ios::in);
    getline(file, tmp);
    outvec.push_back(stol(tmp));
    file.close();
  }
  return outvec;
}

double calc(long diff, long blocksize) {
  double out = (double)(((diff*blocksize)/1024)/1024);
  return out;
}
